/**
 * This file is part of TinyWeatherForecastGermany.
 *
 * Copyright (c) 2020, 2021 Pawel Dube
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.kaffeemitkoffein.tinyweatherforecastgermany;

public class Tag {
    public static final String MAIN = "main activity";
    public static final String SERVICE2 = "UpdateService";
    public static final String ALARMMANAGER = "alarm manager";
    public static final String ONBOOT = "OnBootCompleted-receiver";
    public static final String GB = "gadgetbridge-api";
    public static final String WUBR = "weatherupdate-broadcast-receiver";
    public static final String UPDATEJOBSERVICE = "UpdateJobService";
    public static final String DATABASE = "Database";
    public static final String WARNINGS = "Weather Alerts";
    public static final String WIDGET = "widget";
    public static final String TEXTS = "texts";
    public static final String STATIONS = "stations";
    public static final String RADAR  = "radar";

}
